
public class Sort {

public static void mergeSort(int[] list) {
	mergeSort(list, 0, list.length - 1);

	}

public static void mergeSort(int[] list, int firstHalf, int secondHalf) {
	if (secondHalf - firstHalf > 0) {
	int firstMiddle = (secondHalf + firstHalf) / 2;
	int secondMiddle = firstMiddle + 1;
	mergeSort(list, firstHalf, firstMiddle);
	mergeSort(list, secondMiddle, secondHalf);
	merge(list, firstHalf, firstMiddle, secondMiddle, secondHalf);

	}
	}

public static void merge(int[] list, int firstHalfList, int firstHalfLength, int secondHalfList, int secondHalfLength) {
	int current1 = firstHalfList;
	int current2 = secondHalfList;
	int current3 = 0;
	int[] temp = new int[(firstHalfLength - firstHalfList + 1) + (secondHalfLength - secondHalfList + 1)];

	while (current1 <= firstHalfLength && current2 <= secondHalfLength) {
	if (list[current1] < list[current2])
	temp[current3++] = list[current1++];
	else
	temp[current3++] = list[current2++];

	}

	while (current1 <= firstHalfLength)
	temp[current3++] = list[current1++];
	while (current2 <= secondHalfLength)
	temp[current3++] = list[current2++];

	if (current3 != 0)
	current3 = 0;
	for (int i = firstHalfList; i <= firstHalfLength; i++, current3++) {
	list[i] = temp[current3];

	}

	for (int i = secondHalfList; i <= secondHalfLength; i++, current3++) {
	list[i] = temp[current3];

	}
	}

public static void main(String[] args) {
	int[] list = {2, 3, 2, 5, 6, 1, -2, 3, 14, 12};

	System.out.println("List of numbers before sort:");
	for (int i = 0; i < list.length; i++)
	System.out.print(list[i] + " ");

	mergeSort(list);

	System.out.println("\n\nList of numbers after sort:");
	for (int i = 0; i < list.length; i++)
	System.out.print(list[i] + " ");

	}

}
